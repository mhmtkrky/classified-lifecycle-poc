package org.classified.lifecycle.poc.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import static org.classified.lifecycle.poc.enums.State.*;

@Getter
@RequiredArgsConstructor
public enum Category {
    ESTATE(PENDING, 4),
    SHOPPING(ACTIVE, 3),
    VEHICLE(PENDING, 8),
    OTHER(PENDING, 8);

    private final State state;
    private final int weekly;
}
