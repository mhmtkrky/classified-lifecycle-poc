package org.classified.lifecycle.poc.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.classified.lifecycle.poc.exceptions.AdvertException;

import java.util.Map;
import java.util.HashMap;

@Getter
@RequiredArgsConstructor
public enum State {
    ACTIVE {
        @Override
        public boolean changeAbleState(State state) throws AdvertException {
            if (DEACTIVE != state)
                throw new AdvertException("Active state cannot switch to this state");
            return true;
        }
    },
    PENDING {
        @Override
        public boolean changeAbleState(State state) {
            return true;
        }
    },
    DEACTIVE {
        @Override
        public boolean changeAbleState(State state) throws AdvertException {
            throw new AdvertException("Deactive state cannot switch to this state");
        }
    };

    public abstract boolean changeAbleState(State state) throws AdvertException;

    private static final Map<String, State> MAP = new HashMap<>();
}
