package org.classified.lifecycle.poc.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.classified.lifecycle.poc.enums.Category;
import org.classified.lifecycle.poc.enums.State;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@Entity(name = "ADVERT")
@AllArgsConstructor
@NoArgsConstructor
public class Advert implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TITLE", nullable = false)
    @Length(min = 10, max = 50, message = "Title size must be 2-50")
    private String title;

    @Length(min = 20, max = 50, message = "Detail size must be 2-50")
    @Column(name = "DETAIL")
    private String detail;

    @Column(name = "REPETITIVE")
    private Boolean repetitive;

    @Column(name = "CREATETIME", nullable = false)
    private LocalDateTime createTime;

    @Column(name = "ENDTIME")
    private LocalDateTime endTime;

    @Column(name = "STATE")
    private State state;

    @Column(name = "CATEGORY")
    private Category category;

    @PrePersist
    public void prePersist() {
        this.createTime = LocalDateTime.now();
        int lifeTime = this.getCategory().getWeekly();
        this.endTime = this.createTime.plusWeeks(lifeTime);
        this.state = this.getCategory().getState();
    }
}
