package org.classified.lifecycle.poc.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.classified.lifecycle.poc.enums.State;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@Entity(name = "ADVERT_HISTORY")
@AllArgsConstructor
@NoArgsConstructor
public class AdvertHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "BEFORE_STATE")
    private State beforeState;

    @Column(name = "CURRENT_STATE")
    private State currentState;

    @ManyToOne
    private Advert advert;

    @Column(name = "CREATETIME", nullable = false)
    private LocalDateTime changeTime;

    @PrePersist
    public void prePersist() {
        this.changeTime = LocalDateTime.now();
    }
}
