package org.classified.lifecycle.poc.base;

import org.springframework.http.ResponseEntity;
import org.classified.lifecycle.poc.dto.BaseResponse;

import java.util.List;
import java.util.Optional;

public abstract class BaseController {


    public ResponseEntity<BaseResponse> createPaginationResponse(List<?> response) {

        BaseResponse baseResponse = BaseResponse.builder()
                .success(true)
                .message(response != null && response.size() > 0 ? "Successful" : "Data not found")
                .data(response)
                .build();

        return ResponseEntity.of(Optional.of(baseResponse));
    }

    public ResponseEntity<BaseResponse> createSuccessResponse(Object... data) {
        BaseResponse baseResponse = BaseResponse.builder()
                .success(true)
                .data(data.length > 0 ? data[0] : null)
                .build();
        return ResponseEntity.of(Optional.of(baseResponse));
    }
}
