package org.classified.lifecycle.poc.repository;

import org.classified.lifecycle.poc.entity.AdvertHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AdvertHistoryRepository extends JpaRepository<AdvertHistory, Long> {

    List<AdvertHistory> findByAdvertId(Long advertId);
}
