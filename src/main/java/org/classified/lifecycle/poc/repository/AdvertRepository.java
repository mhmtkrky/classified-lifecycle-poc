package org.classified.lifecycle.poc.repository;

import org.classified.lifecycle.poc.dto.GroupByStateDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.enums.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface AdvertRepository extends JpaRepository<Advert, Long> {

    List<Advert> findByCategoryAndTitleAndDetail(Category category, String title, String detail);

    @Query(value = "select new org.classified.lifecycle.poc.dto.GroupByStateDTO(a.state, count(a.state)) from ADVERT a group by a.state")
    List<GroupByStateDTO> getListGroupByState();
}
