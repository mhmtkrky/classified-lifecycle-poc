package org.classified.lifecycle.poc.mapper;

import org.classified.lifecycle.poc.dto.AdvertHistoryDTO;
import org.classified.lifecycle.poc.entity.AdvertHistory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AdvertHistoryMapper {

    @Mapping(target = "advertId", expression = "java(source.getAdvert() != null ? source.getAdvert().getId() : null)")
    @Mapping(target = "advertTitle", expression = "java(source.getAdvert() != null ? source.getAdvert().getTitle() : null)")
    AdvertHistoryDTO map(AdvertHistory source);

    List<AdvertHistoryDTO> map(List<AdvertHistory> source);
}