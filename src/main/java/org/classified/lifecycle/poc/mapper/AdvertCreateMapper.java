package org.classified.lifecycle.poc.mapper;

import org.classified.lifecycle.poc.dto.AdvertCreateDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AdvertCreateMapper {

    AdvertCreateDTO map(Advert source);

    Advert map(AdvertCreateDTO source);
}
