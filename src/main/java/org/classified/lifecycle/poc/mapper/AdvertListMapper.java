package org.classified.lifecycle.poc.mapper;

import org.classified.lifecycle.poc.dto.AdvertDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AdvertListMapper {

    AdvertDTO map(Advert source);

    Advert map(AdvertDTO source);

    List<AdvertDTO> map(List<Advert> source);
}
