package org.classified.lifecycle.poc.dto;

import lombok.Builder;
import lombok.Data;
import org.classified.lifecycle.poc.enums.State;
import org.classified.lifecycle.poc.enums.Category;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
public class AdvertDTO implements Serializable {
    private Long id;
    private String title;
    private State state;
    private String detail;
    private Category category;
    private boolean repetitive;
    private LocalDateTime endTime;
    private LocalDateTime createTime;
}
