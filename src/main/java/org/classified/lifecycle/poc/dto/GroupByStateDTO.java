package org.classified.lifecycle.poc.dto;

import lombok.Data;
import org.classified.lifecycle.poc.enums.State;

import java.io.Serializable;

@Data
public class GroupByStateDTO implements Serializable {
    private Long count;
    private State state;

    public GroupByStateDTO(State state, Long count) {
        this.count = count;
        this.state = state;
    }
}
