package org.classified.lifecycle.poc.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import org.classified.lifecycle.poc.annotations.BadWords;
import org.classified.lifecycle.poc.annotations.TitleFirstCharacter;
import org.classified.lifecycle.poc.enums.Category;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class AdvertCreateDTO {
    @BadWords
    @TitleFirstCharacter
    @NotNull(message = "Title must not be null")
    @Size(min = 10, max = 50, message = "Title size must be 10-50")
    private String title;

    @Size(min = 20, max = 50, message = "Detail size must be 20-50")
    private String detail;

    @NotNull(message = "Category must not be null, Category must be one of them is ESTATE, SHOPPING, VEHICLE, OTHER")
    private Category category;

    @JsonIgnore
    private boolean repetitive;
}
