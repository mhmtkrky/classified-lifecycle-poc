package org.classified.lifecycle.poc.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.classified.lifecycle.poc.enums.State;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdvertChangeStateDTO {
    @NotNull(message = "State must not be null, State must be one of them is ACTIVE, PENDING, DEACTIVE")
    private State state;
}
