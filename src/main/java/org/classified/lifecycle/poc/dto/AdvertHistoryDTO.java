package org.classified.lifecycle.poc.dto;

import lombok.Data;
import lombok.Builder;
import org.classified.lifecycle.poc.enums.State;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
public class AdvertHistoryDTO implements Serializable {

    private Long advertId;
    private String advertTitle;

    private State beforeState;
    private State currentState;
    private LocalDateTime changeTime;
}
