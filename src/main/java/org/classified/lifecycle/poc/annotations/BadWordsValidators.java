package org.classified.lifecycle.poc.annotations;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;

@RequiredArgsConstructor
public class BadWordsValidators implements ConstraintValidator<BadWords, String> {

    private final Set<String> badwords;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (badwords == null || badwords.isEmpty())
            return true;

        if (value == null)
            return false;

        return badwords.parallelStream().noneMatch(x -> x.contains(value));
    }
}
