package org.classified.lifecycle.poc.annotations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = BadWordsValidators.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface BadWords {
    public String message() default "Bad word can not be used";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}