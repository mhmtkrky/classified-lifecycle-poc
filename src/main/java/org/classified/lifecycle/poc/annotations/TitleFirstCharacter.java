package org.classified.lifecycle.poc.annotations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Constraint(validatedBy = TitleFirstCharacterValidators.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface TitleFirstCharacter {
    public String message() default "Title must start with a character or number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}