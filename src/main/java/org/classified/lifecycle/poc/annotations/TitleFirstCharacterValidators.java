package org.classified.lifecycle.poc.annotations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.classified.lifecycle.poc.constant.AdvertConstant.ADVERT_TITLE_REGEX;

public class TitleFirstCharacterValidators implements ConstraintValidator<TitleFirstCharacter, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null)
            return false;
        Pattern pattern = Pattern.compile(ADVERT_TITLE_REGEX, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(value);
        return matcher.find();
    }
}
