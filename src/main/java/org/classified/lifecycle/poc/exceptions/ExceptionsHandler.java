package org.classified.lifecycle.poc.exceptions;

import org.classified.lifecycle.poc.dto.BaseResponse;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler
    public ResponseEntity<BaseResponse> handle(ConstraintViolationException ex) {
        Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
        String message = constraintViolations.stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(","));
        return createResponse(message);
    }

    @ExceptionHandler
    public ResponseEntity<BaseResponse> handle(MethodArgumentNotValidException ex) {
        List<ObjectError> allErrors = ex.getBindingResult().getAllErrors();
        String message = allErrors.stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(","));
        return createResponse(message);
    }

    @ExceptionHandler
    public ResponseEntity<BaseResponse> handle(AdvertException ex) {
        return createResponse(ex.getMessage());
    }

    private ResponseEntity<BaseResponse> createResponse(String message) {
        return ResponseEntity
                .badRequest()
                .body(
                        BaseResponse.builder().success(false).message(message).build()
                );
    }
}
