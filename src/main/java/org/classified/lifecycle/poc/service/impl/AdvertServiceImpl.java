package org.classified.lifecycle.poc.service.impl;

import lombok.RequiredArgsConstructor;
import org.classified.lifecycle.poc.dto.AdvertChangeStateDTO;
import org.classified.lifecycle.poc.dto.AdvertCreateDTO;
import org.classified.lifecycle.poc.dto.AdvertDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.enums.State;
import org.classified.lifecycle.poc.exceptions.AdvertException;
import org.classified.lifecycle.poc.mapper.AdvertCreateMapper;
import org.classified.lifecycle.poc.mapper.AdvertListMapper;
import org.classified.lifecycle.poc.repository.AdvertRepository;
import org.classified.lifecycle.poc.service.AdvertHistoryService;
import org.classified.lifecycle.poc.service.AdvertService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AdvertServiceImpl implements AdvertService {

    private final AdvertRepository repository;
    private final AdvertListMapper listMapper;
    private final AdvertCreateMapper createMapper;
    private final AdvertHistoryService historyService;

    @Override
    @Cacheable("adverts")
    public List<AdvertDTO> getList() {
        List<Advert> all = repository.findAll();
        return listMapper.map(all);
    }

    @Override
    @Transactional
    @CacheEvict(cacheNames = "adverts", allEntries = true)
    public AdvertDTO save(AdvertCreateDTO dto) {
        AdvertCreateDTO advertCreateDTO = setRepetitive(dto);
        Advert save = repository.save(createMapper.map(advertCreateDTO));
        return listMapper.map(save);
    }

    @Override
    @Transactional
    public AdvertDTO changeState(Long id, AdvertChangeStateDTO stateDTO) throws AdvertException {
        Advert advert = repository.findById(id).orElse(null);

        if (advert == null)
            throw new AdvertException("Not found for id: " + id);

        if (advert.getRepetitive())
            throw new AdvertException("Repetitive record can not be update");

        State beforeState = advert.getState();
        advert.getState().changeAbleState(stateDTO.getState());
        advert.setState(stateDTO.getState());

        Advert save = repository.save(advert);
        saveHistory(save, beforeState);

        return listMapper.map(save);
    }

    private void saveHistory(Advert save, State beforeState) {
        historyService.save(save, beforeState);
    }

    private AdvertCreateDTO setRepetitive(AdvertCreateDTO dto) {
        boolean isRepeat = controlOfRepetitive(dto);
        dto.setRepetitive(isRepeat);
        return dto;
    }

    private boolean controlOfRepetitive(AdvertCreateDTO dto) {
        List<Advert> isUnique = repository.findByCategoryAndTitleAndDetail(dto.getCategory(), dto.getTitle(), dto.getDetail());
        return !(isUnique == null || isUnique.isEmpty());
    }
}
