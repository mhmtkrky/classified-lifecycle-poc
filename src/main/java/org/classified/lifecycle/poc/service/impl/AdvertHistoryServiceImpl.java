package org.classified.lifecycle.poc.service.impl;

import lombok.RequiredArgsConstructor;
import org.classified.lifecycle.poc.dto.AdvertHistoryDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.entity.AdvertHistory;
import org.classified.lifecycle.poc.enums.State;
import org.classified.lifecycle.poc.mapper.AdvertHistoryMapper;
import org.classified.lifecycle.poc.repository.AdvertHistoryRepository;
import org.classified.lifecycle.poc.service.AdvertHistoryService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AdvertHistoryServiceImpl implements AdvertHistoryService {

    private final AdvertHistoryMapper mapper;
    private final AdvertHistoryRepository repository;

    @Override
    @Cacheable("advertsHistory")
    public List<AdvertHistoryDTO> getList(Long id) {
        List<AdvertHistory> result = repository.findByAdvertId(id);
        return mapper.map(result);
    }

    @Override
    @CacheEvict(cacheNames = "advertsHistory", key = "#advert.id")
    public void save(Advert advert, State beforeState) {
        AdvertHistory entity = AdvertHistory
                .builder()
                .advert(advert)
                .currentState(advert.getState())
                .beforeState(beforeState)
                .build();
        repository.save(entity);
    }
}
