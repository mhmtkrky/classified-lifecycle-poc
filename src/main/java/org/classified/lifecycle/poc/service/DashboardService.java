package org.classified.lifecycle.poc.service;

import org.classified.lifecycle.poc.dto.GroupByStateDTO;

import java.util.List;

public interface DashboardService {

    List<GroupByStateDTO> getListGroupByState();
}
