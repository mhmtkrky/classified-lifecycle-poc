package org.classified.lifecycle.poc.service.impl;

import lombok.RequiredArgsConstructor;
import org.classified.lifecycle.poc.dto.GroupByStateDTO;
import org.classified.lifecycle.poc.repository.AdvertRepository;
import org.classified.lifecycle.poc.service.DashboardService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DashboardServiceImpl implements DashboardService {

    private final AdvertRepository repository;

    @Override
    @Cacheable("dashboard")
    public List<GroupByStateDTO> getListGroupByState() {
        return repository.getListGroupByState();
    }
}
