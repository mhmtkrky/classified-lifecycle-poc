package org.classified.lifecycle.poc.service;

import org.classified.lifecycle.poc.dto.AdvertCreateDTO;
import org.classified.lifecycle.poc.dto.AdvertDTO;
import org.classified.lifecycle.poc.dto.AdvertChangeStateDTO;
import org.classified.lifecycle.poc.exceptions.AdvertException;

import java.util.List;

public interface AdvertService {
    List<AdvertDTO> getList();

    AdvertDTO save(AdvertCreateDTO advert);

    AdvertDTO changeState(Long id, AdvertChangeStateDTO state) throws AdvertException;
}
