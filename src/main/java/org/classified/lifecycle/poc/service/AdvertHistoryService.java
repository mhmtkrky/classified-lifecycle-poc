package org.classified.lifecycle.poc.service;

import org.classified.lifecycle.poc.dto.AdvertHistoryDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.enums.State;

import java.util.List;


public interface AdvertHistoryService {
    List<AdvertHistoryDTO> getList(Long id);

    void save(Advert advert, State beforeState);
}
