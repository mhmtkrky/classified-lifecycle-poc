package org.classified.lifecycle.poc.controller;

import lombok.RequiredArgsConstructor;
import org.classified.lifecycle.poc.base.BaseController;
import org.classified.lifecycle.poc.dto.BaseResponse;
import org.classified.lifecycle.poc.service.DashboardService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dashboard/classifieds")
@RequiredArgsConstructor
public class DashboardController extends BaseController {

    private final DashboardService service;

    @GetMapping("/statistics")
    public ResponseEntity<BaseResponse> statistics() {
        return createSuccessResponse(service.getListGroupByState());
    }
}
