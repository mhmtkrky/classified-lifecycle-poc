package org.classified.lifecycle.poc.controller;

import lombok.RequiredArgsConstructor;
import org.classified.lifecycle.poc.base.BaseController;
import org.classified.lifecycle.poc.dto.BaseResponse;
import org.classified.lifecycle.poc.service.AdvertHistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/history")
@RequiredArgsConstructor
public class AdvertHistoryController extends BaseController {

    private final AdvertHistoryService service;

    @GetMapping("/getList/{id}")
    public ResponseEntity<BaseResponse> getList(@PathVariable("id") Long id) {
        return createPaginationResponse(service.getList(id));
    }
}
