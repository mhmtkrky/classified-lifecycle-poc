package org.classified.lifecycle.poc.controller;

import lombok.RequiredArgsConstructor;
import org.classified.lifecycle.poc.base.BaseController;
import org.classified.lifecycle.poc.dto.AdvertCreateDTO;
import org.classified.lifecycle.poc.dto.AdvertChangeStateDTO;
import org.classified.lifecycle.poc.dto.BaseResponse;
import org.classified.lifecycle.poc.exceptions.AdvertException;
import org.classified.lifecycle.poc.service.AdvertService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/advert")
@RequiredArgsConstructor
public class AdvertController extends BaseController {

    private final AdvertService service;

    @GetMapping("/getList")
    public ResponseEntity<BaseResponse> getList() {
        return createPaginationResponse(service.getList());
    }

    @PostMapping("/save")
    public ResponseEntity<BaseResponse> save(@Valid @RequestBody AdvertCreateDTO advert) {
        return createSuccessResponse(service.save(advert));
    }

    @PostMapping("/state/{id}")
    public ResponseEntity<BaseResponse> changeState(@PathVariable("id") Long id, @Valid @RequestBody AdvertChangeStateDTO stateDTO) throws AdvertException {
        return createSuccessResponse(service.changeState(id, stateDTO));
    }
}
