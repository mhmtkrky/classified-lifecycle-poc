package org.classified.lifecycle.poc.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class InterceptorAppConfig implements WebMvcConfigurer {

    @Autowired
    public CustomRequestInterceptor customRequestInterceptor;

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(customRequestInterceptor);
    }
}
