package org.classified.lifecycle.poc.configuration;

import org.classified.lifecycle.poc.service.AdvertService;
import org.classified.lifecycle.poc.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.cache.CacheManager;

@Component
public class CacheScheduler {

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private AdvertService advertService;

    @Autowired
    private DashboardService dashboardService;

    public void evictAllCaches() {
        cacheManager.getCacheNames()
                .forEach(cacheName -> cacheManager.getCache(cacheName).clear());

        advertService.getList();
        dashboardService.getListGroupByState();
    }

    @Scheduled(fixedRate = 1000 * 60, initialDelay = 1000 * 60)
    public void evictAllCachesAtIntervals() {
        evictAllCaches();
    }
}
