package org.classified.lifecycle.poc.configuration;

import lombok.RequiredArgsConstructor;
import org.classified.lifecycle.poc.service.AdvertService;
import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.ehcache.event.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CacheLogger implements CacheEventListener<Object, Object> {

    @Autowired
    private AdvertService service;
    private Logger log = LoggerFactory.getLogger(CustomRequestInterceptor.class);

    @Override
    public void onEvent(CacheEvent<?, ?> cacheEvent) {
        log.info("Advert cache is " + cacheEvent.getType().name());
    }
}
