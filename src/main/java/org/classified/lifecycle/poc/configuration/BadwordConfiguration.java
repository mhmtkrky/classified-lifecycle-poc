package org.classified.lifecycle.poc.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Set;
import java.util.HashSet;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

@Configuration
public class BadwordConfiguration {

    @Bean
    public Set<String> badwords() {

        Set<String> badwords = new HashSet<>();

        InputStream stream = getClass().getClassLoader().getResourceAsStream("Badwords.txt");

        if (stream == null)
            return null;

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = br.readLine()) != null) {
                badwords.add(line);
            }
        } catch (Exception e) {
            return null;
        }

        return badwords;
    }
}
