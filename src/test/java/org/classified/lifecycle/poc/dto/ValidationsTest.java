package org.classified.lifecycle.poc.dto;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.annotations.BadWordsValidators;
import org.classified.lifecycle.poc.exceptions.ExceptionsHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.validation.*;
import java.util.HashSet;
import java.util.Set;

public class ValidationsTest extends BaseUnitTest {

    private Validator validator;
    private ExceptionsHandler handler = new ExceptionsHandler();

    private BadWordsValidators badWordsValidators = new BadWordsValidators(new HashSet<>());

    @BeforeEach
    public void before() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void givenAdvertChangeStateDTO_whenNullState_thenException() {
        AdvertChangeStateDTO dto = new AdvertChangeStateDTO(null);
        Set<ConstraintViolation<AdvertChangeStateDTO>> violations = validator.validate(dto);
        ResponseEntity<BaseResponse> result = handler.handle(new ConstraintViolationException(null, violations));

        Assertions.assertEquals(result.getStatusCode(), HttpStatus.BAD_REQUEST);
    }
}
