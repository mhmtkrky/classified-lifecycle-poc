package org.classified.lifecycle.poc.mapper;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.dto.AdvertCreateDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;

public class AdvertCreateMapperTest extends BaseUnitTest {

    @InjectMocks
    private final AdvertCreateMapper mapper = Mappers.getMapper(AdvertCreateMapper.class);

    @Test
    public void givenAdvertCreateMapper_whenDTOMap_thenReturnEntity() {
        AdvertCreateDTO advertCreateDTO = getAdvertCreateDTO();

        Assertions.assertEquals(advertCreateDTO.getCategory(), mapper.map(advertCreateDTO).getCategory());
    }

    @Test
    public void givenAdvertCreateMapper_whenEntityMap_thenReturnDTO() {
        Advert advert = getAdvert(false);
        Assertions.assertEquals(advert.getCategory(), mapper.map(advert).getCategory());
    }
}
