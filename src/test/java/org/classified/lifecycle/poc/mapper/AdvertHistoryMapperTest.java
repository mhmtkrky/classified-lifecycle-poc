package org.classified.lifecycle.poc.mapper;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.dto.AdvertCreateDTO;
import org.classified.lifecycle.poc.dto.AdvertHistoryDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.entity.AdvertHistory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;


import static java.util.Collections.singletonList;

public class AdvertHistoryMapperTest extends BaseUnitTest {
    @InjectMocks
    private final AdvertHistoryMapper mapper = Mappers.getMapper(AdvertHistoryMapper.class);

    @Test
    public void givenAdvertHistoryMapper_whenEntityListMap_thenReturnListDTO() {
        AdvertHistory advertHistory = getAdvertHistory();
        AdvertHistoryDTO advertHistoryDTO = mapper.map(advertHistory);

        Assertions.assertEquals(singletonList(advertHistoryDTO), mapper.map(singletonList(advertHistory)));
    }

    @Test
    public void givenAdvertHistoryMapper_whenEntityMap_thenReturnDTO() {
        AdvertHistory advertHistory = getAdvertHistory();
        Assertions.assertEquals(advertHistory.getAdvert().getId(), mapper.map(advertHistory).getAdvertId());
        Assertions.assertEquals(advertHistory.getAdvert().getTitle(), mapper.map(advertHistory).getAdvertTitle());
    }
}
