package org.classified.lifecycle.poc.mapper;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.dto.AdvertDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;

import static java.util.Collections.singletonList;

public class AdvertListMapperTest extends BaseUnitTest {
    @InjectMocks
    private final AdvertListMapper mapper = Mappers.getMapper(AdvertListMapper.class);

    @Test
    public void givenAdvertListMapper_whenEntityMap_thenReturnListDTO() {
        Advert advert = getAdvert(false);
        Assertions.assertEquals(advert.getTitle(), mapper.map(advert).getTitle());
    }

    @Test
    public void givenAdvertListMapper_whenDTOMap_thenReturnEntity() {
        AdvertDTO advertDTO = getAdvertDTO();
        Assertions.assertEquals(advertDTO.getTitle(), mapper.map(advertDTO).getTitle());
    }

    @Test
    public void givenAdvertListMapper_whenEntityListMap_thenReturnListDTO() {
        Advert advert = getAdvert(false);
        AdvertDTO dto = mapper.map(advert);
        Assertions.assertEquals(singletonList(dto), mapper.map(singletonList(advert)));
    }
}