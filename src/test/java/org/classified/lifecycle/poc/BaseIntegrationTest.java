package org.classified.lifecycle.poc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.net.URI;

@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
@Tag("integration")
public abstract class BaseIntegrationTest extends ModelDummyDataCreated {

    @Autowired
    public ObjectMapper mapper;

    @Autowired
    public MockMvc mockMvc;

    public static MockHttpServletRequestBuilder get(String urlTemplate, Object... uriVars) {
        return MockMvcRequestBuilders.get(urlTemplate, uriVars);
    }

    public static MockHttpServletRequestBuilder get(URI uri) {
        return MockMvcRequestBuilders.get(uri);
    }

    public static MockHttpServletRequestBuilder post(String urlTemplate, Object... uriVars) {
        return MockMvcRequestBuilders.post(urlTemplate, uriVars);
    }

    public static MockHttpServletRequestBuilder post(URI uri) {
        return MockMvcRequestBuilders.post(uri);
    }

    public static String toJson(Object obj) {
        if (obj == null) return null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
            ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
            return ow.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
