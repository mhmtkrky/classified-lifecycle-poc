package org.classified.lifecycle.poc.exception;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.dto.BaseResponse;
import org.classified.lifecycle.poc.exceptions.AdvertException;
import org.classified.lifecycle.poc.exceptions.ExceptionsHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

public class ExceptionsHandlerTest extends BaseUnitTest {

    @InjectMocks
    private ExceptionsHandler exceptionsHandler;


    @Test
    public void givenHandle_whenAdvertException_thenReturnBaseResponse() {
        AdvertException exception = new AdvertException("messageexcetion");
        ResponseEntity<BaseResponse> handle = exceptionsHandler.handle(exception);

        Assertions.assertEquals(Objects.requireNonNull(handle.getBody()).getMessage(), exception.getMessage());
    }
}
