package org.classified.lifecycle.poc.controller.integration;

import org.classified.lifecycle.poc.BaseIntegrationTest;
import org.classified.lifecycle.poc.entity.AdvertHistory;
import org.classified.lifecycle.poc.repository.AdvertHistoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.annotation.Transactional;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AdvertHistoryControllerTest extends BaseIntegrationTest {

    @MockBean
    private AdvertHistoryRepository repository;

    private String PATH = "/api/history/getList/";


    @Test
    @Transactional
    void givenGetList_whenAdvertId_thenReturnAllDataForAdvertId() throws Exception {
        AdvertHistory advertHistory = getAdvertHistory();
        when(repository.findByAdvertId(1L)).thenReturn(singletonList(advertHistory));

        mockMvc.perform(get(PATH + "1"))
                .andExpect(status().isOk())
                .andReturn();
    }
}
