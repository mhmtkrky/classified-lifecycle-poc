package org.classified.lifecycle.poc.controller;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.dto.AdvertHistoryDTO;
import org.classified.lifecycle.poc.dto.BaseResponse;
import org.classified.lifecycle.poc.service.AdvertHistoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.mockito.Mockito.when;

public class AdvertHistoryControllerTest extends BaseUnitTest {

    @InjectMocks
    private AdvertHistoryController controller;

    @Mock
    private AdvertHistoryService service;


    @Test
    public void givenGetList_whenAdvertId_thenReturnAllDataForAdvertId() {
        List<AdvertHistoryDTO> response = Collections.singletonList(getAdvertHistoryDTO());
        when(service.getList(1L)).thenReturn(response);
        ResponseEntity<BaseResponse> result = controller.getList(1L);
        Assertions.assertEquals(Objects.requireNonNull(result.getBody()).getData(), response);

    }
}
