package org.classified.lifecycle.poc.controller;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.dto.BaseResponse;
import org.classified.lifecycle.poc.dto.GroupByStateDTO;
import org.classified.lifecycle.poc.service.DashboardService;
import org.classified.lifecycle.poc.service.impl.DashboardServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;

import static org.mockito.Mockito.when;

public class DashboardControllerTest extends BaseUnitTest {

    @InjectMocks
    private DashboardController controller;

     @Mock
     private DashboardService service;

    @Test
    public void givenStatics_whenGroupByState_ThenShouldBeAllStateForCount() {
        List<GroupByStateDTO> dtoList = getGroupByStateDTOList();
        when(service.getListGroupByState()).thenReturn(dtoList);

        ResponseEntity<BaseResponse> result = controller.statistics();

        Assertions.assertEquals(dtoList,
                Objects.requireNonNull(result.getBody()).getData());
    }
}
