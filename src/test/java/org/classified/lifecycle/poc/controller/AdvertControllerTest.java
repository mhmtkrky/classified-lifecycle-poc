package org.classified.lifecycle.poc.controller;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.controller.AdvertController;
import org.classified.lifecycle.poc.dto.*;
import org.classified.lifecycle.poc.enums.State;
import org.classified.lifecycle.poc.exceptions.AdvertException;
import org.classified.lifecycle.poc.service.AdvertService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.mockito.Mockito.when;

public class AdvertControllerTest extends BaseUnitTest {

    @InjectMocks
    private AdvertController controller;

    @Mock
    private AdvertService service;

    @Test
    void givenGetList_whenAllAdvert_thenReturnAllDataForAdvertId() {
        List<AdvertDTO> list = Collections.singletonList(getAdvertDTO());

        when(service.getList()).thenReturn(list);
        ResponseEntity<BaseResponse> result = controller.getList();

        Assertions.assertEquals(Objects.requireNonNull(result.getBody()).getData(), list);
    }

    @Test
    void givenSave_whenCreateDTO_thenSave() {
        AdvertDTO advertDTO = getAdvertDTO();
        AdvertCreateDTO advertCreateDTO = getAdvertCreateDTO();

        when(service.save(advertCreateDTO)).thenReturn(advertDTO);
        ResponseEntity<BaseResponse> result = controller.save(advertCreateDTO);

        Assertions.assertEquals(Objects.requireNonNull(result.getBody()).getData(), advertDTO);
    }

    @Test
    void givenChangeState_whenStateDTO_thenChangeState() throws AdvertException {
        AdvertDTO advertDTO = getAdvertDTO();
        AdvertChangeStateDTO advertChangeStateDTO = getAdvertChangeStateDTO(State.ACTIVE);

        when(service.changeState(1L, advertChangeStateDTO)).thenReturn(advertDTO);
        ResponseEntity<BaseResponse> result = controller.changeState(1L, advertChangeStateDTO);

        Assertions.assertEquals(Objects.requireNonNull(result.getBody()).getData(), advertDTO);
    }
}
