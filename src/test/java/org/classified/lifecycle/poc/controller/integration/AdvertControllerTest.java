package org.classified.lifecycle.poc.controller.integration;

import org.classified.lifecycle.poc.BaseIntegrationTest;
import org.classified.lifecycle.poc.dto.AdvertChangeStateDTO;
import org.classified.lifecycle.poc.dto.AdvertCreateDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.entity.AdvertHistory;
import org.classified.lifecycle.poc.enums.Category;
import org.classified.lifecycle.poc.enums.State;
import org.classified.lifecycle.poc.repository.AdvertHistoryRepository;
import org.classified.lifecycle.poc.repository.AdvertRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;

import java.time.LocalDateTime;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AdvertControllerTest extends BaseIntegrationTest {

    @MockBean
    private AdvertHistoryRepository historyRepository;

    @MockBean
    private AdvertRepository advertRepository;

    private String PATH = "/api/advert";

    @Test
    public void givenGetList_whenAllAdvert_thenReturnAllData() throws Exception {
        Advert advert = getAdvert(false);
        Page<Advert> pageObject = new PageImpl<>(singletonList(advert));
        when(advertRepository.findAll(PageRequest.of(0, 100, Sort.by("title")))).thenReturn(pageObject);

        mockMvc.perform(get(PATH + "/getList"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void givenSave_whenNullValueRequest_thenException() throws Exception {
        AdvertCreateDTO createDTO = AdvertCreateDTO
                .builder()
                .title(null)
                .detail(null)
                .category(null)
                .build();

        mockMvc.perform(post(PATH + "/save")
                .content(toJson(createDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void givenSave_whenBadSizeRequest_thenException() throws Exception {
        AdvertCreateDTO createDTO = AdvertCreateDTO
                .builder()
                .title("less10")
                .detail("less20")
                .category(Category.ESTATE)
                .build();

        mockMvc.perform(post(PATH + "/save")
                .content(toJson(createDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void givenSave_whenTitleNotCorrect_thenException() throws Exception {
        AdvertCreateDTO createDTO = AdvertCreateDTO
                .builder()
                .title("!moreThan10ButNotCorrect")
                .detail("moreThan20moreThan201")
                .category(Category.ESTATE)
                .build();

        mockMvc.perform(post(PATH + "/save")
                .content(toJson(createDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }


    @Test
    public void givenSave_whenTitleBadWord_thenException() throws Exception {
        AdvertCreateDTO createDTO = AdvertCreateDTO
                .builder()
                .title("rdjiqmfdqy")
                .detail("moreThan20moreThan201")
                .category(Category.ESTATE)
                .build();

        mockMvc.perform(post(PATH + "/save")
                .content(toJson(createDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void givenSave_whenRequestCorrect_thenSave() throws Exception {
        AdvertCreateDTO createDTO = AdvertCreateDTO
                .builder()
                .title("moreThan101")
                .detail("moreThan20moreThan201")
                .category(Category.ESTATE)
                .build();

        when(advertRepository.save(any())).thenReturn(getAdvert(false));

        mockMvc.perform(post(PATH + "/save")
                .content(toJson(createDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void givenChangeState_whenRequestCorrect_thenSave() throws Exception {
        AdvertChangeStateDTO request = AdvertChangeStateDTO
                .builder()
                .state(State.ACTIVE)
                .build();

        Advert advert = getAdvert(false);
        advert.setId(1L);

        when(advertRepository.findById(1L)).thenReturn(Optional.of(advert));
        when(advertRepository.save(any())).thenReturn(advert);
        when(historyRepository.save(any())).thenReturn(getAdvertHistory());

        mockMvc.perform(post(PATH + "/state/1")
                .content(toJson(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void givenChangeState_whenIdNotFound_thenException() throws Exception {
        AdvertChangeStateDTO request = AdvertChangeStateDTO
                .builder()
                .state(State.ACTIVE)
                .build();

        when(advertRepository.findById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(post(PATH + "/state/1")
                .content(toJson(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void givenChangeState_whenIsRepetitiveTrue_thenException() throws Exception {
        AdvertChangeStateDTO request = AdvertChangeStateDTO
                .builder()
                .state(State.ACTIVE)
                .build();

        Advert advert = getAdvert(true);

        when(advertRepository.findById(1L)).thenReturn(Optional.of(advert));

        mockMvc.perform(post(PATH + "/state/1")
                .content(toJson(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void givenChangeState_whenNotChangeable_thenException() throws Exception {
        AdvertChangeStateDTO request = AdvertChangeStateDTO
                .builder()
                .state(State.PENDING)
                .build();

        Advert advert = getAdvert(false);
        advert.setState(State.DEACTIVE);

        when(advertRepository.findById(1L)).thenReturn(Optional.of(advert));

        mockMvc.perform(post(PATH + "/state/1")
                .content(toJson(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void givenChangeState_whenRequestIsNull_thenException() throws Exception {
        AdvertChangeStateDTO request = AdvertChangeStateDTO
                .builder()
                .state(null)
                .build();

        mockMvc.perform(post(PATH + "/state/1")
                .content(toJson(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }
}
