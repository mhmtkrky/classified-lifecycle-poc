package org.classified.lifecycle.poc.controller.integration;

import org.classified.lifecycle.poc.BaseIntegrationTest;
import org.classified.lifecycle.poc.dto.BaseResponse;
import org.classified.lifecycle.poc.dto.GroupByStateDTO;
import org.classified.lifecycle.poc.repository.AdvertRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DashboardControllerTest extends BaseIntegrationTest {

    @MockBean
    private AdvertRepository repository;

    private String PATH = "/dashboard/classifieds/statistics";


    @Test
    @Transactional
    void givenStatics_whenGroupByState_ThenShouldBeAllStateForCount() throws Exception {
        List<GroupByStateDTO> group = getGroupByStateDTOList();
        when(repository.getListGroupByState()).thenReturn(group);

        MvcResult mvcResult = mockMvc.perform(get(PATH))
                .andExpect(status().isOk())
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        BaseResponse baseResponse = mapper.readValue(response.getContentAsString(), BaseResponse.class);
        Assertions.assertEquals(toJson(baseResponse.getData()), toJson(group));
    }
}
