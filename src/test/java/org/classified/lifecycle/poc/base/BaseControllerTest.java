package org.classified.lifecycle.poc.base;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.dto.BaseResponse;
import org.classified.lifecycle.poc.entity.Advert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class BaseControllerTest extends BaseUnitTest {

    @Spy
    private BaseController controller;

    @Test
    void givenBaseController_whenPaginationResponse_thenReturnPaginationBaseResponse() {
        List<Advert> response = Collections.singletonList(getAdvert(true));
        ResponseEntity<BaseResponse> result = controller.createPaginationResponse(response);
        Assertions.assertEquals(Objects.requireNonNull(result.getBody()).getData(), response);
    }

    @Test
    void givenBaseController_whenPaginationNull_thenReturnNullBaseResponse() {
        ResponseEntity<BaseResponse> result = controller.createPaginationResponse(null);
        Assertions.assertEquals(Objects.requireNonNull(result.getBody()).getMessage(), "Data not found");
    }

    @Test
    void givenBaseController_whenSuccessResponse_thenReturnSuccessResponse() {
        Advert advert = getAdvert(true);
        ResponseEntity<BaseResponse> result = controller.createSuccessResponse(advert);
        Assertions.assertEquals(Objects.requireNonNull(result.getBody()).getData(), advert);
    }

}
