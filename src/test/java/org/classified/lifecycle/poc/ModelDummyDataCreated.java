package org.classified.lifecycle.poc;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.classified.lifecycle.poc.dto.*;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.entity.AdvertHistory;
import org.classified.lifecycle.poc.enums.Category;
import org.classified.lifecycle.poc.enums.State;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ModelDummyDataCreated {
    private final ObjectMapper mapper = new ObjectMapper();


    public List<GroupByStateDTO> getGroupByStateDTOList() {
        List<GroupByStateDTO> state = new ArrayList<>();
        state.add(getGroupByStateDTO(State.ACTIVE, 5L));
        state.add(getGroupByStateDTO(State.PENDING, 115L));
        state.add(getGroupByStateDTO(State.DEACTIVE, 150L));
        return state;
    }

    public GroupByStateDTO getGroupByStateDTO(State state, Long count) {
        return new GroupByStateDTO(state, count);
    }


    public AdvertHistory getAdvertHistory() {
        return AdvertHistory
                .builder()
                .advert(getAdvert(false))
                .beforeState(State.ACTIVE)
                .currentState(State.DEACTIVE)
                .changeTime(LocalDateTime.now())
                .build();
    }

    public Advert getAdvert(boolean isRepeat) {
        return Advert
                .builder()
                .state(State.PENDING)
                .repetitive(isRepeat)
                .title("AdvertTitle1")
                .detail("AdvertDetail1")
                .category(Category.ESTATE)
                .createTime(LocalDateTime.now())
                .build();
    }

    public AdvertDTO getAdvertDTO() {
        return AdvertDTO
                .builder()
                .repetitive(false)
                .title("AdvertTitle1")
                .detail("AdvertDetail1")
                .createTime(LocalDateTime.now())
                .category(Category.ESTATE)
                .build();

    }

    public AdvertHistoryDTO getAdvertHistoryDTO() {
        return AdvertHistoryDTO
                .builder()
                .advertId(1L)
                .advertTitle("title")
                .beforeState(State.ACTIVE)
                .currentState(State.DEACTIVE)
                .changeTime(LocalDateTime.now())
                .build();
    }

    public AdvertCreateDTO getAdvertCreateDTO() {
        return AdvertCreateDTO
                .builder()
                .title("AdvertTitle1")
                .detail("AdvertDetail1")
                .category(Category.ESTATE)
                .build();

    }

    public AdvertChangeStateDTO getAdvertChangeStateDTO(State state) {
        return AdvertChangeStateDTO
                .builder()
                .state(state)
                .build();
    }
}
