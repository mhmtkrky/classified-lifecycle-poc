package org.classified.lifecycle.poc.annotation;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.annotations.TitleFirstCharacterValidators;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

public class TitleFirstCharacterValidatorsTest extends BaseUnitTest {

    @InjectMocks
    private TitleFirstCharacterValidators validators;

    @Test
    public void givenTitle_whenNotCorrect_thenReturnFalse() {
        Assertions.assertFalse(validators.isValid("!title123", null));
    }

    @Test
    public void givenTitle_whenCorrect_thenReturnTrue() {
        Assertions.assertTrue(validators.isValid("title123", null));
    }
}
