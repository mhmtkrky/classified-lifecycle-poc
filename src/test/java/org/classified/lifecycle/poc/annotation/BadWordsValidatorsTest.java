package org.classified.lifecycle.poc.annotation;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.annotations.BadWordsValidators;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

public class BadWordsValidatorsTest extends BaseUnitTest {

    private BadWordsValidators validators;
    Set<String> badwords = new HashSet<>();

    @BeforeEach
    public void before() {
        validators = new BadWordsValidators(badwords);
    }

    @Test
    void givenBadWordValid_whenBadWordListEmpty_thenReturnTrue() {
        Assertions.assertTrue(validators.isValid("test", null));
    }

    @Test
    void givenBadWordValid_whenValueIsNull_thenReturnFalse() {
        badwords.add("test");
        Assertions.assertFalse(validators.isValid(null, null));
    }

    @Test
    void givenBadWordValid_whenValueIsBadWords_thenReturnFalse() {
        badwords.add("badword");
        Assertions.assertFalse(validators.isValid("badword", null));
    }

    @Test
    void givenBadWordValid_whenValueIsNotBadWords_thenReturnFalse() {
        badwords.add("badword");
        Assertions.assertTrue(validators.isValid("notbadword", null));
    }
}
