package org.classified.lifecycle.poc.configuration;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

public class CustomRequestInterceptorTest extends BaseUnitTest {

    @Spy
    private CustomRequestInterceptor interceptor;

    HttpServletRequest httpServletRequest;
    HttpServletResponse httpServletResponse;


    @BeforeEach
    public void before() {
        httpServletRequest = new MockHttpServletRequest();
        httpServletResponse = new MockHttpServletResponse();
    }

    @Test
    public void interceptor_setStartTime() {
        Assertions.assertTrue(interceptor.preHandle(httpServletRequest, httpServletResponse, null));
    }
}
