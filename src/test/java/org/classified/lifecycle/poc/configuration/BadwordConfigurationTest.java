package org.classified.lifecycle.poc.configuration;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class BadwordConfigurationTest extends BaseUnitTest {

    private BadwordConfiguration configuration = new BadwordConfiguration();


    @Test
    void givenBadWord_whenBadWordsStringList_thenReturnStringList() {
        Set<String> badwords = configuration.badwords();

        Assertions.assertNotNull(badwords);
    }
}
