package org.classified.lifecycle.poc.configuration;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class InterceptorAppConfigTest extends BaseUnitTest {

    @InjectMocks
    private InterceptorAppConfig interceptorAppConfig;

    @Mock
    private InterceptorRegistry registry;

    @Test
    public void test() {
        interceptorAppConfig.addInterceptors(registry);

        verify(registry, times(1))
                .addInterceptor(any());
    }
}
