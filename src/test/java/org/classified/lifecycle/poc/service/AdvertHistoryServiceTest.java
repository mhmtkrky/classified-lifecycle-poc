package org.classified.lifecycle.poc.service;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.dto.AdvertHistoryDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.entity.AdvertHistory;
import org.classified.lifecycle.poc.enums.State;
import org.classified.lifecycle.poc.mapper.AdvertHistoryMapper;
import org.classified.lifecycle.poc.repository.AdvertHistoryRepository;
import org.classified.lifecycle.poc.service.impl.AdvertHistoryServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

public class AdvertHistoryServiceTest extends BaseUnitTest {

    @InjectMocks
    private AdvertHistoryServiceImpl service;

    @Mock
    private AdvertHistoryMapper historyMapper;

    @Mock
    private AdvertHistoryRepository repository;


    @Test
    public void givenGetList_whenAdvertId_thenReturnAllDataForAdvertId() {
        AdvertHistory advertHistory = getAdvertHistory();
        when(repository.findByAdvertId(anyLong())).thenReturn(Collections.singletonList(advertHistory));
        AdvertHistoryDTO advertHistoryDTO = getAdvertHistoryDTO();
        when(historyMapper.map(singletonList(advertHistory))).thenReturn(singletonList(advertHistoryDTO));
        List<AdvertHistoryDTO> list = service.getList(1L);
        Assertions.assertEquals(singletonList(advertHistoryDTO), list);
    }

    @Test
    public void givenSave_whenAdvertAndBeforeState_thenSaveBeforeStateForAdvert() {
        Advert advert = getAdvert(false);
        when(repository.save(any())).thenReturn(any());
        service.save(advert, State.ACTIVE);
        verify(repository, times(1)).save(any());
    }
}
