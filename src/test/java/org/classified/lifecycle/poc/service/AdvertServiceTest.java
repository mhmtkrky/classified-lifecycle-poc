package org.classified.lifecycle.poc.service;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.dto.AdvertCreateDTO;
import org.classified.lifecycle.poc.dto.AdvertDTO;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.enums.State;
import org.classified.lifecycle.poc.exceptions.AdvertException;
import org.classified.lifecycle.poc.mapper.AdvertCreateMapper;
import org.classified.lifecycle.poc.mapper.AdvertListMapper;
import org.classified.lifecycle.poc.repository.AdvertRepository;
import org.classified.lifecycle.poc.service.impl.AdvertServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class AdvertServiceTest extends BaseUnitTest {

    @InjectMocks
    private AdvertServiceImpl service;
    @Mock
    private AdvertRepository repository;
    @Mock
    private AdvertListMapper listMapper;
    @Mock
    private AdvertCreateMapper createMapper;
    @Mock
    private AdvertHistoryService historyService;


    @Test
    public void givenGetList_whenAllAdvert_thenReturnAllData() {
        Advert advert = getAdvert(false);

        when(repository.findAll()).thenReturn(singletonList(advert));
        AdvertDTO advertDTO = getAdvertDTO();
        when(listMapper.map(singletonList(advert))).thenReturn(singletonList(advertDTO));

        List<AdvertDTO> response = service.getList();
        assertEquals(singletonList(advertDTO), response);
    }


    @Test
    public void givenSave_whenAdvertCreateDTO_thenSaveAdvert() {
        AdvertCreateDTO advertCreateDTO = getAdvertCreateDTO();
        Advert advert = getAdvert(false);

        when(repository.findByCategoryAndTitleAndDetail(any(), any(), any())).thenReturn(null);
        when(createMapper.map(advertCreateDTO)).thenReturn(advert);
        when(repository.save(advert)).thenReturn(advert);

        AdvertDTO advertDTO = getAdvertDTO();
        when(listMapper.map(advert)).thenReturn(advertDTO);

        assertEquals(advertDTO, service.save(advertCreateDTO));
    }

    @Test
    public void givenChangeState_whenNullAdvert_thenException() {
        when(repository.findById(anyLong())).thenReturn(Optional.empty());
        AdvertException exception = assertThrows(AdvertException.class, () -> service.changeState(1L, getAdvertChangeStateDTO(State.ACTIVE)));
        assertEquals("Not found for id: 1", exception.getMessage());
    }

    @Test
    public void givenChangeState_whenRepetitiveIsTrueRecord_thenExcepcion() {
        Advert advert = getAdvert(true);
        when(repository.findById(anyLong())).thenReturn(Optional.of(advert));

        AdvertException exception = assertThrows(AdvertException.class, () -> service.changeState(1L, getAdvertChangeStateDTO(State.ACTIVE)));
        assertEquals("Repetitive record can not be update", exception.getMessage());
    }

    @Test
    public void givenChangeState_whenNotChangeableState_thenException() {
        Advert advert = getAdvert(false);
        advert.setState(State.ACTIVE);
        when(repository.findById(anyLong())).thenReturn(Optional.of(advert));

        AdvertException exception = assertThrows(AdvertException.class, () -> service.changeState(1L, getAdvertChangeStateDTO(State.PENDING)));
        assertEquals("Active state cannot switch to this state", exception.getMessage());
    }

    @Test
    public void givenChangeState_whenCorrectState_thenReturnSave() throws AdvertException {
        Advert advert = getAdvert(false);
        advert.setState(State.ACTIVE);
        when(repository.findById(anyLong())).thenReturn(Optional.of(advert));

        when(repository.save(advert)).thenReturn(advert);
        doNothing().when(historyService).save(any(), any());
        AdvertDTO advertDTO = getAdvertDTO();
        when(listMapper.map(advert)).thenReturn(advertDTO);

        Assertions.assertEquals(advertDTO, service.changeState(1L, getAdvertChangeStateDTO(State.DEACTIVE)));
    }

}
