package org.classified.lifecycle.poc.service;

import org.classified.lifecycle.poc.BaseUnitTest;
import org.classified.lifecycle.poc.dto.GroupByStateDTO;
import org.classified.lifecycle.poc.repository.AdvertRepository;
import org.classified.lifecycle.poc.service.impl.DashboardServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;

import static org.mockito.Mockito.when;

public class DashboardServiceTest extends BaseUnitTest {

    @InjectMocks
    private DashboardServiceImpl service;

    @Mock
    private AdvertRepository repository;


    @Test
    public void givenStatics_whenGroupByState_ThenShouldBeAllStateForCount() {
        List<GroupByStateDTO> dtoList = getGroupByStateDTOList();
        when(repository.getListGroupByState()).thenReturn(dtoList);
        Assertions.assertEquals(dtoList, service.getListGroupByState());
    }
}
