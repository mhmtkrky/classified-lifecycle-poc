FROM openjdk:11
LABEL maintainer="mehmet-karakaya42@hotmail.com"
ADD target/classified-lifecycle-poc-1.0-SNAPSHOT.jar classified-lifecycle-poc-1.0-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "classified-lifecycle-poc-1.0-SNAPSHOT.jar"]