package org.classified.lifecycle.poc.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.classified.lifecycle.poc.dto.AdvertHistoryDTO;
import org.classified.lifecycle.poc.dto.AdvertHistoryDTO.AdvertHistoryDTOBuilder;
import org.classified.lifecycle.poc.entity.AdvertHistory;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-01-22T22:21:39+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.17 (Oracle Corporation)"
)
@Component
public class AdvertHistoryMapperImpl implements AdvertHistoryMapper {

    @Override
    public AdvertHistoryDTO map(AdvertHistory source) {
        if ( source == null ) {
            return null;
        }

        AdvertHistoryDTOBuilder advertHistoryDTO = AdvertHistoryDTO.builder();

        advertHistoryDTO.beforeState( source.getBeforeState() );
        advertHistoryDTO.currentState( source.getCurrentState() );
        advertHistoryDTO.changeTime( source.getChangeTime() );

        advertHistoryDTO.advertId( source.getAdvert() != null ? source.getAdvert().getId() : null );
        advertHistoryDTO.advertTitle( source.getAdvert() != null ? source.getAdvert().getTitle() : null );

        return advertHistoryDTO.build();
    }

    @Override
    public List<AdvertHistoryDTO> map(List<AdvertHistory> source) {
        if ( source == null ) {
            return null;
        }

        List<AdvertHistoryDTO> list = new ArrayList<AdvertHistoryDTO>( source.size() );
        for ( AdvertHistory advertHistory : source ) {
            list.add( map( advertHistory ) );
        }

        return list;
    }
}
