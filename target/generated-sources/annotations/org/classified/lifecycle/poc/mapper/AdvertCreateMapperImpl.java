package org.classified.lifecycle.poc.mapper;

import javax.annotation.processing.Generated;
import org.classified.lifecycle.poc.dto.AdvertCreateDTO;
import org.classified.lifecycle.poc.dto.AdvertCreateDTO.AdvertCreateDTOBuilder;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.entity.Advert.AdvertBuilder;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-01-22T22:21:40+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.17 (Oracle Corporation)"
)
@Component
public class AdvertCreateMapperImpl implements AdvertCreateMapper {

    @Override
    public AdvertCreateDTO map(Advert source) {
        if ( source == null ) {
            return null;
        }

        AdvertCreateDTOBuilder advertCreateDTO = AdvertCreateDTO.builder();

        advertCreateDTO.title( source.getTitle() );
        advertCreateDTO.detail( source.getDetail() );
        advertCreateDTO.category( source.getCategory() );
        if ( source.getRepetitive() != null ) {
            advertCreateDTO.repetitive( source.getRepetitive() );
        }

        return advertCreateDTO.build();
    }

    @Override
    public Advert map(AdvertCreateDTO source) {
        if ( source == null ) {
            return null;
        }

        AdvertBuilder advert = Advert.builder();

        advert.title( source.getTitle() );
        advert.detail( source.getDetail() );
        advert.repetitive( source.isRepetitive() );
        advert.category( source.getCategory() );

        return advert.build();
    }
}
