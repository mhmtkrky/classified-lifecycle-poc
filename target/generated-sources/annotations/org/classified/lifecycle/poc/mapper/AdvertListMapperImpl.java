package org.classified.lifecycle.poc.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.classified.lifecycle.poc.dto.AdvertDTO;
import org.classified.lifecycle.poc.dto.AdvertDTO.AdvertDTOBuilder;
import org.classified.lifecycle.poc.entity.Advert;
import org.classified.lifecycle.poc.entity.Advert.AdvertBuilder;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-01-22T22:21:40+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.17 (Oracle Corporation)"
)
@Component
public class AdvertListMapperImpl implements AdvertListMapper {

    @Override
    public AdvertDTO map(Advert source) {
        if ( source == null ) {
            return null;
        }

        AdvertDTOBuilder advertDTO = AdvertDTO.builder();

        advertDTO.id( source.getId() );
        advertDTO.title( source.getTitle() );
        advertDTO.state( source.getState() );
        advertDTO.detail( source.getDetail() );
        advertDTO.category( source.getCategory() );
        if ( source.getRepetitive() != null ) {
            advertDTO.repetitive( source.getRepetitive() );
        }
        advertDTO.endTime( source.getEndTime() );
        advertDTO.createTime( source.getCreateTime() );

        return advertDTO.build();
    }

    @Override
    public Advert map(AdvertDTO source) {
        if ( source == null ) {
            return null;
        }

        AdvertBuilder advert = Advert.builder();

        advert.id( source.getId() );
        advert.title( source.getTitle() );
        advert.detail( source.getDetail() );
        advert.repetitive( source.isRepetitive() );
        advert.createTime( source.getCreateTime() );
        advert.endTime( source.getEndTime() );
        advert.state( source.getState() );
        advert.category( source.getCategory() );

        return advert.build();
    }

    @Override
    public List<AdvertDTO> map(List<Advert> source) {
        if ( source == null ) {
            return null;
        }

        List<AdvertDTO> list = new ArrayList<AdvertDTO>( source.size() );
        for ( Advert advert : source ) {
            list.add( map( advert ) );
        }

        return list;
    }
}
